create database faculdade;
\c faculdade;

CREATE TABLE IF NOT EXISTS FUNCIONARIO (
 CODIGO_FUNCIONARIO INTEGER CONSTRAINT PK_CODIGO_FUNCIONARIO PRIMARY KEY,
     NOME_FUNCIONARIO VARCHAR(255) NOT NULL,
    DATA_ADMISSAO DATE NOT NULL,
    DATA_DEMISSAO DATE
);

CREATE SEQUENCE FUNCIONARIO_SEQ
  start 1000
  increment 4;


CREATE TABLE IF NOT EXISTS TITULACAO (
 CODIGO_TITULACAO INTEGER CONSTRAINT PK_CODIGO_TITULACAO PRIMARY KEY,
 NOME_TITULACAO VARCHAR(25) NOT NULL,
 DESCRICAO_TITULACAO VARCHAR(255)
);


CREATE SEQUENCE TITULACAO_SEQ
  start 1
  increment 1;



CREATE TABLE IF NOT EXISTS PROFESSOR (
    CODIGO_FUNCIONARIO INTEGER NOT NULL,
    MATRICULA_PROFESSOR INTEGER NOT NULL,
    CODIGO_TITULACAO INTEGER,
    PRIMARY KEY (CODIGO_FUNCIONARIO, MATRICULA_PROFESSOR),
    FOREIGN KEY (CODIGO_TITULACAO)
    REFERENCES TITULACAO(CODIGO_TITULACAO)
);


CREATE SEQUENCE PROFESSOR_SEQ
  start 2000
  increment 5;
