#!/bin/bash

shuf /home/NOMES.TXT | paste -d, - /home/DATAS.TXT | sed -e 's/\//-/g;s/\r//g' | \
    awk -F, '{printf "insert into funcionario values(nextval(\x27funcionario_seq\x27),\x27%s\x27, to_date(\x27%s\x27, \x27DD-MM-YY\x27));\n",$1,$2}' | \
    head > /home/func.sql
psql -d faculdade -h projeto_final_grupo01_pgcesar1_1 -U postgres -f /home/func.sql
