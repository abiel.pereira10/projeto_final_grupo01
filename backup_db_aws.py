#!/usr/bin/python3

import boto3
import os

from datetime import datetime

def update_aws():
    access_key_id = os.environ['AWS_ACCESS_KEY_ID']
    secret_access_key = os.environ['AWS_SECRET_ACCESS_KEY']

    s3 = boto3.client('s3', region_name='us-east-1',
                    aws_access_key_id=access_key_id,
                    aws_secret_access_key=secret_access_key)

    bucket = s3.create_bucket(Bucket='backup-desafio-final-amilton2')

    now = datetime.now()

    s3.upload_file(
        Filename='/home/backupcesar/faculdade.sql',
        Bucket='backup-desafio-final-amilton2',
        Key='faculdade.sql'+now.strftime("%H:%M:%S")
    )

if __name__ == "__main__":
    update_aws()

#print ("Hello meow")
