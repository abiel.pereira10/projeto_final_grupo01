# Projeto_Final_grupo01



## Getting started

`git clone https://gitlab.com/abiel.pereira10/projeto_final_grupo01.git`


### 1- Subir os containers
`sudo docker-compose up --build`

### 2- A cada 1min os dados serao gerados e enviados para o container projeto_final_grupo01_pgcesar1_1. Para verificar:

`sudo docker exec -it projeto_final_grupo01_pgcesar1_1 bash`

`psql -d faculdade -U postgres`

`select * from funcionario;`

### 3- A cada 5min é feito um backup do db faculdade para o container projeto_final_grupo01_pgcesar1_backup_1. Para verificar:

`sudo docker exec -it projeto_final_grupo01_pgcesar1_backup_1 bash`

`ls /home/backupcesar/faculdade.sql`

### 4- O container projeto_final_grupo01_awscesar_1  é responsavel por enviar o backup para um Um bucket no Amazon S3. 

### TODO enviar o backup para um Um bucket no Amazon S3