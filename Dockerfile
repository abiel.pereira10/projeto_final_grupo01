FROM debian

RUN apt update && apt install -y postgresql-client cron

COPY ./DATAS.TXT /home
COPY ./NOMES.TXT /home
COPY ./gen-func-data.sh /home
COPY  gen-data-cron.sh /etc/cron.d/gen-data-cron.sh

RUN chmod 0744 /home/gen-func-data.sh
RUN echo 'projeto_final_grupo01_pgcesar1_1:5432:faculdade:postgres:cesar' > ~/.pgpass
RUN chmod 600 ~/.pgpass
RUN chmod 0644 /etc/cron.d/gen-data-cron.sh
RUN /bin/bash /etc/cron.d/gen-data-cron.sh
RUN touch /var/log/cron.log

CMD cron && tail -f /var/log/cron.log
